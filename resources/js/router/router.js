import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import HomeComponent from '../components/HomeComponent'
import AboutComponent from '../components/AboutComponent'
import RegisterComponent from '../components/RegisterPageComponent'
import LoginComponent from '../components/LoginPageComponent'

const routes = [
  { path: '/', component: HomeComponent },
  { path: '/about', component: AboutComponent },
  { path: '/register', component: RegisterComponent },
  { path: '/login', component: LoginComponent }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  mode: 'history',
  routes // short for `routes: routes`
})

export default router